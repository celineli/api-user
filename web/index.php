<?php

// importation de classes
use Michelf\Markdown;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// chargement de l'autoloading
require_once __DIR__.'/../vendor/autoload.php';

// fixtures (données de test)
$users = [
	['id' => 0, 'name' => 'lorem'],
	['id' => 1, 'name' => 'ipsum'],
	['id' => 2, 'name' => 'foo'],
];

// création d'une nouvelle appli
$app = new Silex\Application();

// activation du mode déboggage
$app['debug'] = true;

// connexion à la base de données
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'    => 'pdo_mysql',
        'host'      => 'localhost',
        'dbname'    => 'api_user',
        'user'      => 'root',
        'password'  => '123',
        'charset'   => 'utf8',
    ),
));

// home du site
$app->get('/', function() {
	// @todo utiliser twig plutôt que du html en dur
	$htmlHead = '<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title></title>
  </head>
  <body>
';

	$htmlTail = '  </body>
</html>
';

	// lecture du fichier README.md
	$text = file_get_contents('../README.md');

	// transformation du markdonw en html
	$html = Markdown::defaultTransform($text);

	return $htmlHead . $html . $htmlTail;
});

// liste des users
$app->get('/api/users/', function() use ($app) {
	$sql = "SELECT * FROM user";
    $users = $app['db']->fetchAll($sql);

	return $app->json($users);
});

// détails d'un user
$app->get('/api/users/{id}', function($id) use ($app) {
	$sql = "SELECT * FROM user WHERE id = ?";
    $user = $app['db']->fetchAssoc($sql, [(int) $id]);

	return $app->json($user);
});

// création d'un nouveau user
$app->post('/api/users/', function(Request $request) use ($app) {
	$firstname = $request->get('firstname');
	$lastname = $request->get('lastname');
	$email = $request->get('email');
	$birthday = $request->get('birthday');
	$github = $request->get('github');
	$sex = $request->get('sex');
	$pet = $request->get('pet');

	if ($pet == 'true') {
		$pet = true;
	} elseif ($pet == 'false') {
		$pet = false;
	}

	$app['db']->insert('user', [
		'firstname' => $firstname,
		'lastname' => $lastname,
		'email' => $email,
		'birthday' => $birthday,
		'github' => $github,
		'sex' => $sex,
		'pet' => $pet,
	]);

	$lastId = $app['db']->lastInsertId();

	return $lastId;
});

// suppression d'un user
$app->delete('/api/users/{id}', function($id) use ($app) {
	$resultat = $app['db']->delete('user', [
		'id' => (int) $id,
	]);

	if ($resultat) {
		$return = 204;
	} else {
		$return = 500;
	}

	return new Response('', $return);
});

// démarrage de l'appli
$app->run();
